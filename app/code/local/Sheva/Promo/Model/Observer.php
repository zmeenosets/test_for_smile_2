<?php

class Sheva_Promo_Model_Observer
{
    const PRODUCT_ATTRIBUTE_DISCOUNT = 'product_attribute_discount';

    /**
     * Add new rule
     *
     * @param Varien_Event_Observer $observer
     */
    public function adminhtmlBlockSalesruleActionsPrepareform(Varien_Event_Observer $observer) {
        $form = $observer->getForm();
        $fieldset = $form->getElement('action_fieldset');
        // Create new field product attribute discount
        $fieldAttributes = $fieldset->addField('attribute_discount', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Attribute for discount'),
            'name'      => 'attribute_discount',
        ));
        $attributeOptions = Mage::getSingleton('sheva_promo/options')->toOptionArray();
        $fieldAttributes->addElementValues($attributeOptions, true);
        // Edit form field simple action
        $fieldSimpleAction = $form->getElement('simple_action');
        $fieldSimpleAction->addElementValues(array(
            self::PRODUCT_ATTRIBUTE_DISCOUNT => 'Fixed amount from product attribute',
        ));
        $fieldSimpleAction->setOnchange('onchangeSimpleAction(this)');
        $fieldSimpleAction->setAfterElementHtml('
                        <script>
                        function onchangeSimpleAction(elem) {
                            if(elem.value == "product_attribute_discount") {
                                $("rule_attribute_discount").up(1).show();
                            }
                            else {
                                $("rule_attribute_discount").up(1).hide();
                            }
                        }
                        document.observe("dom:loaded", function() {
                            onchangeSimpleAction($("rule_simple_action"));
                        });
                        </script>
                    ');
    }

    /**
     * Apply discount
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesruleValidatorProcess(Varien_Event_Observer $observer)
    {
        /** @var $item Mage_Sales_Model_Quote_Item */
        $item = $observer->getEvent()->getItem();
        /**@var Mage_SalesRule_Model_Rule $rule */
        $rule = $observer->getEvent()->getRule();

        if($rule->getSimpleAction() == self::PRODUCT_ATTRIBUTE_DISCOUNT) {
            /*
             * Other parameters discount
             * in this task we dont need,
             * we use only product attribute discount
             * $discountAmount = $rule->getDiscountAmount();
             * $discountQty = $rule->getDiscountQty();
            */
            $attributeDiscount = $rule->getAttributeDiscount();
            // Load custom attribute without all product data
            $resource = $item->getProduct()->getResource();
            $discountProduct = $resource->getAttributeRawValue($item->getProductId(), $attributeDiscount, Mage::app()->getStore());
            //$discountProduct = $item->getProduct()->getData($attributeDiscount);
            if(!empty($discountProduct)) {
                // Discount percent
                $item->setDiscountPercent(($discountProduct / $item->getPrice()) * 100);
                // Set result
                $result = $observer->getResult();
                $result->setDiscountAmount($discountProduct);
                $result->setBaseDiscountAmount($discountProduct);
            }
        }
    }
}