<?php

class Sheva_Promo_Model_Options
{
    public function toOptionArray()
    {
        $result = array();
        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
            //->addFieldToFilter('additional_table.is_used_for_promo_rules', 1)
            ->getItems();
        foreach ($attributes as $attribute) {
                $result[] = array(
                    'value' => $attribute->getAttributeCode(),
                    'label' => Mage::helper('adminhtml')->__($attribute->getFrontendLabel())
                );
        }
        return $result;
    }
}